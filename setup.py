import sys
import setuptools
from setuptools.command.test import test as TestCommand

class PyTest(TestCommand):
	user_options = [
		('pytest-args=', 'a', "Arguments to pass to pytest")
	]
	
	def initialize_options(self):
		super().initialize_options()
		self.pytest_args = []
	
	def run_tests(self):
		import pytest
		sys.exit(pytest.main(self.pytest_args))

tests_require = ['pytest']

with open('README.md', 'r') as fh:
	long_description = fh.read()

setuptools.setup(
	name = 'ezfinder',
	version = '0.1.0',
	author = "valtron",
	description = "Nicer APIs for writing `MetaPathFinder`s",
	long_description = long_description,
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/ezfinder',
	packages = ['ezfinder'],
	python_requires = '>=3.6',
	install_requires = [],
	tests_require = tests_require,
	extras_require = { 'test': tests_require },
	cmdclass = { 'test': PyTest },
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Programming Language :: Python :: 3.8',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: MIT License',
	],
)

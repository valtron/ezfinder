import sys
import importlib
from pathlib import Path
from typing import Callable, Any, Optional, Sequence, Union

LoaderFn = Callable[[Path], Any]

class FileFinder(importlib.abc.MetaPathFinder):
	# Can't annotate with the right type because of this issue:
	# https://github.com/python/mypy/issues/708
	loader_fn: Any
	ext: str
	
	def __init__(self, loader_fn: LoaderFn, ext: str) -> None:
		self.loader_fn = loader_fn
		self.ext = ext
	
	def find_module(self, fullname: str, path: Optional[Sequence[Union[bytes, str]]]) -> Optional['FileLoader']:
		# TODO: If importing a.b.c, where a.b -> a/b.ext
		# it should make the loader for a.b, then getattr c
		filename = fullname.replace('.', '/')
		filepath = Path('{}.{}'.format(filename, self.ext))
		if not filepath.exists():
			return None
		return FileLoader(fullname, filepath, self.loader_fn)

class FileLoader(importlib.abc.Loader):
	fullname: str
	filepath: Path
	loader_fn: Any
	
	def __init__(self, fullname: str, filepath: Path, loader_fn: LoaderFn) -> None:
		self.fullname = fullname
		self.filepath = filepath
		self.loader_fn = loader_fn
	
	def load_module(self, fullname: str) -> Any:
		if self.fullname not in sys.modules:
			sys.modules[self.fullname] = self.loader_fn(self.filepath)
		return sys.modules[self.fullname]

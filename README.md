# EZFinder

Nicer APIs for writing `MetaPathFinder`s. It makes writing crazy things (like importing non-Python files) a breeze!

## Install

```
pip install ezfinder
```

## Use

### Example: Import CSV files

```
import sys, ezfinder, pandas as pd

sys.meta_path.append(ezfinder.FileFinder(pd.read_csv, ext = 'csv'))

# Assuming you have a CSV file in data/table.csv...
from data import table

assert isinstance(table, pd.DataFrame)
```
